import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { ProduitsPage } from '../pages/produits/produits';
import { MenusPage } from '../pages/menus/menus';
import { PipesModule } from '../pipes/pipes.module';
import { ApiProvider } from '../providers/api/api';
import { HttpModule } from '@angular/http';
import { SignInPage } from '../pages/sign-in/sign-in';
import { RegisterPage } from '../pages/register/register';
import { LogoutPage } from '../pages/logout/logout';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { CartPage } from '../pages/cart/cart';
import { CartProvider } from '../providers/cart/cart';

import { IonicStorageModule } from '@ionic/storage';
import { LocationProvider } from '../providers/location/location';
import { Geolocation } from '@ionic-native/geolocation';


//Pipe currency in french
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    ProduitsPage,
    MenusPage,
    SignInPage,
    RegisterPage,
    LogoutPage,
    CartPage
  ],
  imports: [
    HttpModule,
    PipesModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    ProduitsPage,
    MenusPage,
    SignInPage,
    RegisterPage,
    LogoutPage,
    CartPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    AuthServiceProvider,
    CartProvider,
    LocationProvider,
    Geolocation,
  ]
})
export class AppModule {}
