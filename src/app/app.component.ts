import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { ProduitsPage } from '../pages/produits/produits';
import { MenusPage } from '../pages/menus/menus';
import { SignInPage } from '../pages/sign-in/sign-in';
import { RegisterPage } from '../pages/register/register';
import { LogoutPage } from '../pages/logout/logout';
import { CartPage } from '../pages/cart/cart';
import { CartProvider } from '../providers/cart/cart';

import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { LocationProvider } from '../providers/location/location';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = HomePage;
  loader:any;
  activePage:any;
  status: boolean = false;
  titleMenu:string = "Bienvenue";

  pages: Array<{title: string, component: any, icon: string, fn?: any;}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public alertCtrl:AlertController, events:Events, public cartProvider:CartProvider, public storage: Storage, public loadingCtrl: LoadingController, public locationProvider: LocationProvider) {

    // this.presentLoading();
 
    // this.platform.ready().then(() => {
 
    //   this.storage.get('introShown').then((result) => {
 
    //     if(result){
    //       this.rootPage = 'HomePage';
    //     } else {
    //       this.rootPage = 'IntroPage';
    //       this.storage.set('introShown', true);
    //     }
 
    //     this.loader.dismiss();
 
    //   });
 
    // });

    this.pages = [
      { title: 'Accueil', component: HomePage, icon: "ios-home-outline"},
      { title: 'A propos', component: AboutPage, icon: "ios-information-circle-outline", fn: () => this.showPrompt() },
      { title: 'Nos Produits', component: ProduitsPage, icon: "ios-pizza-outline"},
      { title: 'Nos Menus', component: MenusPage, icon: "pizza"},

    ];
    this.status = true;

    events.subscribe('user:loggedin',()=>{
      this.pages = [
        { title: 'Accueil', component: HomePage, icon: "ios-home-outline"},
        { title: 'A propos', component: AboutPage, icon: "ios-information-circle-outline", fn: () => this.showPrompt() },
        { title: 'Nos Produits', component: ProduitsPage, icon: "pizza"},
        { title: 'Déconnexion', component: LogoutPage, icon: "ios-exit-outline"},
      ];
      this.status = false;
    });

    events.subscribe('user:loggedout',()=>{
      this.pages = [
        { title: 'Accueil', component: HomePage, icon: "home"},
        { title: 'A propos', component: AboutPage, icon: "ios-information-circle-outline", fn: () => this.showPrompt() },
        { title: 'Nos Produits', component: ProduitsPage, icon: "pizza"},
      ];
      this.status = true;
    });

    this.activePage = this.pages[0];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  presentLoading() {
 
    this.loader = this.loadingCtrl.create({
      content: "Chargement..."
    });
 
    this.loader.present();
 
  }

  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Login',
      message: "Entrez vos coordonnées",
      inputs: [
        {
          name: 'lastname',
          placeholder: 'Nom'
        },
        {
          name: 'firstname',
          placeholder: 'Prénom'
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Valider',
          handler: data => {
            this.nav.setRoot(AboutPage, {
              lastname: data.lastname,
              firstname: data.firstname
            })
          }
        }
      ]
    });
    prompt.present();
  }

  login(){
    this.nav.setRoot(SignInPage)
  }

  signup(){
    this.nav.setRoot(RegisterPage)
  }

  showCart(){
    this.titleMenu = "Mon Panier";
    this.nav.setRoot(CartPage)
  }
  

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.titleMenu = page.title;
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  checkActive(page){
    return page == this.activePage;
  }
}
