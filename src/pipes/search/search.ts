import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'search',
})

export class SearchPipe implements PipeTransform {

  transform(products: any[], terms: string): any[] {
    if (!products) return [];
    if (!terms) return products;
    terms = terms.toLowerCase();
    return products.filter(product => {
      return product.name.toLowerCase().includes(terms); // only filter product name
    });
  }

}
