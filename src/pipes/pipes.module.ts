import { NgModule } from '@angular/core';
import { FirstLetterUppercasePipe } from './first-letter-uppercase/first-letter-uppercase';
import { SearchPipe } from './search/search';
@NgModule({
	declarations: [FirstLetterUppercasePipe,
    SearchPipe],
	imports: [],
	exports: [FirstLetterUppercasePipe,
    SearchPipe],
})
export class PipesModule {}
