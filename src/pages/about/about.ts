import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home'
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  lastname:string
  firstname:string

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.lastname = this.navParams.get('lastname')
    this.firstname = this.navParams.get('firstname')

    if (!this.firstname || !this.lastname){
      // affiche le prompte

      // si annulle ou met rien du tout
      this.navCtrl.setRoot(HomePage)
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

}
