import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { regexValidators } from '../validators/validator';

import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';



/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public events: Events) {

    this.credentialsForm = this.formBuilder.group({
      email: [
        '', Validators.compose([
          Validators.pattern(regexValidators.email),
          Validators.required
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.pattern(regexValidators.password),
          Validators.required
        ])
      ],
    });
  }

  onSignIn() {

    if (this.credentialsForm.valid) {
        console.log('Email: ' +
        this.credentialsForm.controls['email'].value);
        console.log('Password: ' +
        this.credentialsForm.controls['password'].value);
    }
    this.events.publish('user:loggedin');
    this.navCtrl.setRoot(HomePage);
  }

  onForgotPassword() {
    console.log('SignInPage: onForgotPassword()');
  }

  signup(){
    this.navCtrl.push(RegisterPage);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

}
