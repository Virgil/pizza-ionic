import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the MenusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menus',
  templateUrl: 'menus.html',
})
export class MenusPage {

  menus: [any]

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiProvider: ApiProvider, public cartProvider:CartProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenusPage');
    this.apiProvider.getMenus().subscribe(menus=> {
      this.menus = menus
    })
  }

}
