import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { CartProvider } from '../../providers/cart/cart';

/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  cart:any = { products:[]};
  productsInCart: Object[] = [];


  constructor(public navCtrl: NavController, public navParams: NavParams, public apiProvider: ApiProvider, public cartProvider: CartProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
    console.log(JSON.stringify(this.cartProvider.productsInCart));
  }
  
}
