import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { CartProvider } from '../../providers/cart/cart';
import { Product } from '../../models/product'


/**
 * Generated class for the ProduitsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-produits',
  templateUrl: 'produits.html',
})
export class ProduitsPage {


  products:Array<Product>;
  menus: [any]
  categories: [any]

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiProvider: ApiProvider, public cartProvider:CartProvider) {
  }


  
  ionViewDidLoad() {
    this.apiProvider.getProducts().subscribe(products=> {
      this.products = products
    })
    this.apiProvider.getCategories().subscribe(categories=> {
      this.categories = categories
    })
    this.apiProvider.getMenus().subscribe(menus=> {
      this.menus = menus
    })
    console.log('ionViewDidLoad ProduitsPage');
  }

  onSegmentChanged(event) { 
    this.apiProvider.getProductsbyId(event.value).subscribe(products=> {
      this.products = products
    })
  }

}
