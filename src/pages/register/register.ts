import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { regexValidators } from '../validators/validator';

import { HomePage } from '../home/home';
import { SignInPage } from '../sign-in/sign-in';

import { AlertController } from 'ionic-angular';


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public events: Events, public alertCtrl: AlertController) {

    this.credentialsForm = this.formBuilder.group({
      lastname: [
        '', Validators.compose([
          Validators.pattern(regexValidators.lastname),
          Validators.required
        ])
      ],
      firstname: [
        '', Validators.compose([
          Validators.pattern(regexValidators.firstname),
          Validators.required
        ])
      ],
      email: [
        '', Validators.compose([
          Validators.pattern(regexValidators.email),
          Validators.required
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.pattern(regexValidators.password),
          Validators.required
        ])
      ],
    });
  }


  showAlert() {
    const alert = this.alertCtrl.create({
      title: ('Bienvenue'+ this.credentialsForm.controls['firstname'].value),
      subTitle: 'Vous venez de créer votre compte',
      buttons: ['OK']
    });
    alert.present();
  }

  onSignUp() {

    if (this.credentialsForm.valid) {
        console.log('Lastname: ' +
        this.credentialsForm.controls['lastname'].value);
        console.log('Firstname: ' +
        this.credentialsForm.controls['firstname'].value);
        console.log('Email: ' +
        this.credentialsForm.controls['email'].value);
        console.log('Password: ' +
        this.credentialsForm.controls['password'].value);
    }
    this.events.publish('user:loggedin');
    this.navCtrl.setRoot(HomePage);
    this.showAlert();
  }

  login(){
    this.navCtrl.push(SignInPage);
  }
}

  

