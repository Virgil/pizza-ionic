import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { ProduitsPage } from '../produits/produits';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public alertCtrl:AlertController) {
  }

  dataCover = [{ image: "https://loremflickr.com/g/500/240/pizza,restaurant/all" },
               { image: "https://loremflickr.com/g/500/240/pizza/all" },
               { image: "https://loremflickr.com/g/500/240/drink/all" }]

  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Login',
      message: "Entrez vos coordonnées",
      inputs: [
        {
          name: 'lastname',
          placeholder: 'Nom'
        },
        {
          name: 'firstname',
          placeholder: 'Prénom'
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Valider',
          handler: data => {
            this.navCtrl.push(AboutPage, {
              lastname: data.lastname,
              firstname: data.firstname
            })
          }
        }
      ]
    });
    prompt.present();
  }

  gotoproduct(){
    this.navCtrl.push(ProduitsPage);
  }

}
