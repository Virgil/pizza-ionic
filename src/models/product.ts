export interface Product{
    id: number,
    quantity?: number,
    title: string,
    price: number,

}