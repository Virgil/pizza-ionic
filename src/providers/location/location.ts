import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { Injectable} from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

//declare var google;
/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {

  // @ViewChild('map') mapElement: ElementRef;
  // map: any;

  constructor(public http: Http, public toastCtrl: ToastController, private geolocation: Geolocation) {
    console.log('Hello LocationProvider Provider');
  }



  getLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      let location = 'latitude ' + resp.coords.latitude + ' longitude ' + resp.coords.longitude;
      let toast = this.toastCtrl.create({
        message: location,
        duration: 4000
      });
      toast.present();
      console.log("on passe ok")

    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }




  // loadMap() {

  //   this.geolocation.getCurrentPosition().then((position) => {

  //     let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

  //     let mapOptions = {
  //       center: latLng,
  //       zoom: 15,
  //       mapTypeId: google.maps.MapTypeId.ROADMAP
  //     }

  //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  //   }, (err) => {
  //     console.log(err);
  //   });

  // }

}
