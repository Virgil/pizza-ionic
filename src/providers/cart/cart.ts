import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Product } from '../../models/product'
import { AlertController } from 'ionic-angular';


/*
  Generated class for the CartProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CartProvider {

  productsInCart: any[] = [];
  products:Array<Product>;

  totalcart = 0
  totalprice = 0



  constructor(public http: Http, public alertCtrl: AlertController) {
    console.log('Hello CartProvider Provider');
  }

  //ajouter un produit à la carte
  addToCart(product) {
    if (this.productsInCart.indexOf(product)=== -1){
      product.quantityInCart = 1;
      product.quantity = 1;
      this.productsInCart.push(product);
      console.log(JSON.stringify(this.productsInCart));
    }else{
      this.productsInCart[this.productsInCart.indexOf(product)].quantity++
    }
    this.totalqty();
    this.prixtotal();
  }

  //quantity +1 produit
  increment(product) {
    this.productsInCart[this.productsInCart.indexOf(product)].quantity++
    this.totalqty();
    this.prixtotal();
  }


  //quantity -1 produit & delete si inférieur à 1
  decrement(product) {
    if(product.quantity > 1) {
      this.productsInCart[this.productsInCart.indexOf(product)].quantity--
      this.totalqty();
      this.prixtotal();
    }else{
      this.deletePrompt(product)
    }
  }

  // delete product
  delete(product) {
    this.productsInCart[this.productsInCart.indexOf(product)]
    if(product.quantityInCart > -1){
      this.productsInCart.splice(product, 1);
      this.totalqty();
      this.prixtotal();
    }
  }

  // Prompt confirm delete !
  deletePrompt(product) {
    let alert = this.alertCtrl.create({
      title: 'Supprimer',
      message: 'Voulez-vous vraiment supprimer ce produit ?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Valider',
          handler: () => {
            this.delete(product)
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }


  totalqty() {
    this.totalcart = 0
    this.productsInCart.forEach(product => {
      this.totalcart += product.quantity
    });
  }

  prixtotal() {
    this.totalprice = 0
    this.productsInCart.forEach(product => {
      this.totalprice += product.price * product.quantity
    });
  }


  


}
