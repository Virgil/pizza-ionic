import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map'

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  headers = new Headers();
  apiUrl = '/api';
  prod = false;

  constructor(public http: Http) {
    console.log('Hello ApiProvider Provider');
    if(this.prod){
      this.apiUrl = 'http://fast-badlands-48562.herokuapp.com/api/1.0'; 
    }else{
      this.apiUrl = '/api'; 
    }
  }



  getProducts() {
    this.headers.append('Content-Type', 'application/json');
    return this.http.get(this.apiUrl + '/products', { headers: this.headers }).map(response => response.json());
  }

  getProductsbyId(id) {
    this.headers.append('Content-Type', 'application/json');
    return this.http.get(this.apiUrl + '/products/categories/' + id, { headers: this.headers }).map(response => response.json());
  }

  getCategories() {
    this.headers.append('Content-Type', 'application/json');
    return this.http.get(this.apiUrl + '/categories', { headers: this.headers }).map(response => response.json());
  }

  getMenus() {
    this.headers.append('Content-Type', 'application/json');
    return this.http.get(this.apiUrl + '/menus', { headers: this.headers }).map(response => response.json());
  }

}
